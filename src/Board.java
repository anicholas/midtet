import javax.microedition.lcdui.game.GameCanvas;
import javax.microedition.midlet.MIDlet;
import javax.microedition.lcdui.game.Sprite;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.Canvas;
import java.io.IOException;
import java.io.InputStream;
import javax.microedition.media.MediaException;
import javax.microedition.media.Manager;
import javax.microedition.media.Player;
import javax.microedition.media.control.VolumeControl; 


public class Board extends GameCanvas implements Runnable
{
    Tetris  parent;
    Display dpy;
    private int width;
    private int height;
    private int volume;
    private Rectangle boundsPlayArea;
    private Rectangle boundsScorePanel;
    private Rectangle boundsNextPiece;
    private static final int INITIAL_SPEED = 500;
    private Piece       currentPiece;
    private Piece       nextPiece;
    private NextPiece   nextPieceDisplay;
    private ScoreBoard  scoreBoard;
    private Matrix      matrix;
    private Image       levelImg;
    private Image       squaresImg;
    private boolean music,sound;
    private int startLevel;
    private int gameType;
    private GameOverPaint gop;
    private long startTime;
    private int elapsedTime;
    private final int STATE_INPLAY = 0;
    private final int STATE_GAMEOVER = 1;
    private final int STATE_END = 2;
    private int gameStatus = STATE_INPLAY;
    private Thread thread;
    private Player audioPlayer;
    private VolumeControl vol;
    private Graphics graphics;

    public Board(Tetris parent)
    {
        super(false);
        setFullScreenMode(true);
        graphics = getGraphics();
        graphics.setColor( 0,  0, 0 );
        graphics.fillRect( 0, 0, getWidth(), getHeight() );
        volume = 50;
        width = getWidth();
        height = getHeight();
        boundsPlayArea = new Rectangle(width/2 - 60, height/2-60, 60, 120);
        boundsScorePanel = new Rectangle(width/2 + 4, height/2 + 4, 10, 10);
        boundsNextPiece = new Rectangle(width/2 + 14,height/2-30, 36, 30);
        gop = new GameOverPaint();
        this.parent = parent;
        dpy = Display.getDisplay(parent);
        startLevel = 1;
        gameType = 0;

        try {
            String name ="/images/bg.png";
            levelImg = Image.createImage(name);
            String name2 ="/images/squares.png";
            squaresImg = Image.createImage(name2);
        } catch (IOException e) {
            System.out.println("images stuffed");
        }

        matrix     = new Matrix(boundsPlayArea,squaresImg);
        scoreBoard = new ScoreBoard(width,height);
        reset();
        nextPieceDisplay = new NextPiece(nextPiece.testRelocate(0,-2,3),nextPiece.getColor(),squaresImg,boundsNextPiece);
    }

    public void reset()
    {
        gameStatus = STATE_INPLAY;
        matrix.clear();
        if (gameType==1){
            scoreBoard.setGameType(gameType);
            matrix.randomFillBottomRows(startLevel*2);
            startTime = System.currentTimeMillis();
        }
        nextPiece    = new Piece();
        currentPiece = nextPiece;
        scoreBoard.reset(startLevel);
    }

    public void setType(int i)
    {
        gameType = i;
        scoreBoard.setGameType(gameType);
    }

    public void setVolume(int i){
        volume = i;
    }

    public void setLevel(int i){
        startLevel = i + 1;
        scoreBoard.setLevel(startLevel);
    }

    public void setSound(int i)
    {
        if(i==GameMenu.TOGGLE_ON){
            sound = true;
            System.out.println("sound on");
        }else{
            sound = false;
        }
    }

    public void setMusic(int i)
    {
        if(i==GameMenu.TOGGLE_ON){
            music = true;
            System.out.println("music on");
        }else{
            music = false;
        }
    }

    private void playMusic()
    {
        if(music){
            System.out.println(volume);
            try {
                InputStream is = getClass().getResourceAsStream("/music/tetris.mid");
                audioPlayer = Manager.createPlayer(is, "audio/midi");
                audioPlayer.realize();
                vol = (VolumeControl)audioPlayer.getControl("VolumeControl");
                if(vol != null) {
                    vol.setLevel(volume);
                }
                audioPlayer.prefetch();
                audioPlayer.start();
            }
            catch(IOException ioe) {
                System.out.println(ioe);
            }
            catch(MediaException e) {
                System.out.println(e);
            }
        }
    }

    private synchronized void getNextPiece()
    {
        currentPiece = nextPiece;
        nextPiece    = new Piece();
        int dx = -2;
        int dy = 3;
        int r = 0;
        switch(nextPiece.getColor()){
            case Piece.L_PIECE:
                r++;
                break;
            case Piece.P_PIECE:
                r++;
                dy++;
                break;
            case Piece.T_PIECE:
                break;
            case Piece.I_PIECE:
                dy++;
                break;
            case Piece.O_PIECE:
                break;
            case Piece.Z_PIECE:
                break;
            case Piece.S_PIECE:
                break;
        }
        nextPieceDisplay.setPoints(nextPiece.testRelocate(r,dx,dy),nextPiece.getColor());
    }

    private synchronized void dropAll()
    {
        boolean dropAvailble = true;
        do{
            dropAvailble = drop();
        }while(dropAvailble);
    }

    public synchronized boolean drop()
    {
        Point[] test = currentPiece.testRelocate(currentPiece.getRotation(),0, 1);
        boolean result;

        if(validateMove(test)) {
            currentPiece.relocate(currentPiece.getRotation(), 0, 1);
            result = true;
        } else {
            result = false;
            if(matrix.isOffTop(currentPiece.getCurrentTetramino())){
                gameStatus = STATE_GAMEOVER;
            }
            getNextPiece();
            int rc = matrix.checkForCompleteRows();
            scoreBoard.update(0,rc);
        }
        if(result)paint();
        return result;
    }

    public void paint()
    {
        graphics.setColor(0,0,0);
        graphics.fillRect(0,0,width,height);
        graphics.drawImage(levelImg,width/2 - 64,height/2 - 64,Graphics.TOP|Graphics.LEFT);

        if (gameType==1){
            elapsedTime = (int)(System.currentTimeMillis() - startTime);
            scoreBoard.draw(graphics,elapsedTime/1000);
        }else{
            scoreBoard.draw(graphics,0);
        }
        matrix.draw(graphics);
        nextPieceDisplay.draw(graphics);

        switch(gameStatus){
            case STATE_GAMEOVER:
                if(!gop.update())gameStatus=STATE_END;
                break;
        }
        flushGraphics();
    }

    private boolean validateMove(Point[] newP)
    {
        if(newP==null){
            return false;
        }
        matrix.removePiece(currentPiece.getCurrentTetramino());
        if(matrix.canPlacePiece(newP) == -1) {
            matrix.placePiece(newP,currentPiece.getColor());
            return true;
        } else {
            matrix.placePiece(currentPiece.getCurrentTetramino(),currentPiece.getColor());
            return false;
        }
    }

    private void gameOver()
    {
        if(audioPlayer != null){
            try{
                audioPlayer.stop();
            } catch (MediaException me) { }
        }
        gop.reset();
        parent.switchTo(Tetris.MENU);
    }

    public void run()
    {
        Thread mythread = Thread.currentThread();
        while (mythread == thread) {
            try {
                if (mythread == thread) {
                }
                try {
                    mythread.sleep(getDelay());
                    switch (gameStatus){
                        case STATE_INPLAY:
                            drop();
                            break;
                        default:
                            paint();
                    }
                } catch (java.lang.InterruptedException e) {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private int getDelay()
    {
        switch (gameStatus){
            case STATE_INPLAY:
                return INITIAL_SPEED - scoreBoard.level*40;
            case STATE_GAMEOVER:
                return GameOverPaint.DELAY;
            case STATE_END:
                return INITIAL_SPEED;
            default:
                return INITIAL_SPEED;
        }
    }

    protected void showNotify()
    {
        thread = new Thread(this);
        thread.start();
        playMusic();
    }

    protected void hideNotify()
    {
        thread = null;
    }

    public void keyPressed(int code)
    {
        if(gameStatus==STATE_GAMEOVER)return;
        if(gameStatus==STATE_END)gameOver();

        int dx = 0;
        int dy = 0;
        int dr = currentPiece.getRotation();
        int game = getGameAction(code);

        switch (game) {
            case Canvas.UP:
                dropAll();
                return;
            case Canvas.DOWN:
                dy = 1;
                break;
            case Canvas.LEFT:
                dx = -1;
                break;
            case Canvas.RIGHT:
                dx = 1;
                break;
            case Canvas.FIRE:
                dr = currentPiece.getRotationIndex(Piece.ROTATE_LEFT);
                break;
            default:
                return;
        }

        Point[] test = currentPiece.testRelocate(dr, dx, dy);

        if(validateMove(test)) {
            currentPiece.relocate(dr, dx, dy);
            paint();
        }
    }

    class GameOverPaint
    {
        private int r;
        public static final int DELAY = 30;

        public GameOverPaint()
        {
            reset();
        }

        public void reset()
        {
            r = Matrix.NO_OF_ROWS + Matrix.TOP_GUTTER - 1;
        }

        public boolean update()
        {
            if (r < Matrix.TOP_GUTTER)return false;
            for(int c = 0; c < Matrix.NO_OF_COLUMNS;c++){
                matrix.fillSlot(c,r,Piece.X_PIECE);
            }
            r--;
            return true;
        }
    }
}
/* vim: set ts=4 sw=4 nu expandtab: */

