import java.util.*;
import javax.microedition.lcdui.*;
import javax.microedition.lcdui.Image;
import java.io.IOException;
import javax.microedition.lcdui.game.Sprite;
import javax.microedition.lcdui.Graphics;

public class SplashScreen extends Canvas
{
    private int width;
    private int height;
    private Display display;
    private Displayable next;
    private Timer timer = new Timer();
    private Sprite logo;
    private int spriteFrame;

    public SplashScreen( Display display, Displayable next)
    {
        this.display = display;
        this.next    = next;
        setFullScreenMode(true);
        width = getWidth();
        height = getHeight();
        spriteFrame = 0;

        try {
            String name ="/images/derob.png";
            logo = new Sprite(Image.createImage(name),41,14);
        } catch (IOException e) {
            logo = null;
        }

        logo.setPosition(width/2-20,height/2-14);
        display.setCurrent( this );
    }

    protected void keyPressed( int keyCode )
    {
        dismiss();
    }

    protected void paint( Graphics g )
    {
        g.setFont(Font.getFont(Font.FACE_PROPORTIONAL,Font.STYLE_PLAIN,Font.SIZE_SMALL));
        logo.setFrame(spriteFrame);
        g.setColor(0,0,0);
        g.fillRect(0,0,width,height);
        logo.paint(g);
        g.setColor(255,255,255);
        g.drawString("2004 A.Nicholas",width/2,height/2+4,Graphics.HCENTER|Graphics.TOP);
    }

    protected void showNotify()
    {
        timer.schedule( new StepLogo(), 500 );
        timer.schedule( new StepLogo(), 1000 );
        timer.schedule( new StepLogo(), 1500 );
        timer.schedule( new CountDown(), 2000 );
    }

    private void dismiss()
    {
        timer.cancel();
        display.setCurrent( next );
    }

    private class CountDown extends TimerTask
    {
        public void run(){
            dismiss();
        }
    }

    private class StepLogo extends TimerTask
    {
        public void run(){
            spriteFrame++;
            repaint();
        }
    }
}
/* vim: set ts=4 sw=4 nu expandtab: */
