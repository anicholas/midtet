import java.util.Vector;
import java.util.Enumeration;
import javax.microedition.lcdui.Graphics;
import java.util.Random;
import java.lang.ArithmeticException;

public class StarField
{
    private Vector galaxy;
    private int displayWidth,displayHeight;
    private int speed;

    public StarField(int density, int s, int w, int h)
    {
        speed = s;
        displayWidth = w;
        displayHeight = h;
        galaxy = new Vector();
        createStars(density);
    }

    public void draw(Graphics g)
    {
        g.setColor(235,235,235);
        for (Enumeration e = galaxy.elements() ; e.hasMoreElements() ;) {
            Star s = (Star)e.nextElement();
            s.step();
            if(s.x > displayWidth || s.x < 0 || s.y < 0 || s.y > displayHeight){
                galaxy.removeElement(s);
                createStars(1);
            }else{
                g.drawRect(s.x,s.y,0,0);
            }
        }
    }

    private void createStars(int no)
    {
        Random random = new Random();
        for(int i = 0; i < no; i++){
            try{
                int x = displayWidth/4 + Math.abs(random.nextInt()%(displayWidth/2));
                int y = displayHeight/4 + Math.abs(random.nextInt()%(displayHeight/2));
                int dx = (x-displayWidth/2)/4;
                int dy = (y-displayWidth/2)/4;
                if (dx == 0 && dy ==0)dx = 3;
                galaxy.addElement(new Star(x,y,dx,dy));
            }catch (ArithmeticException e){
                System.out.println(e);
            }
        }
    }
}

class Star extends Point
{

    private  int dx,dy;

    public Star(int x, int y, int dx, int dy)
    {
        setLocation(x,y);
        setVelocity(dx,dy);
    }

    private void setVelocity(int dx, int dy)
    {
        this.dx = dx;
        this.dy = dy;
    }

    public void step()
    {
        setLocation(x+dx,y+dy);
    }

}
/* vim: set ts=4 sw=4 nu expandtab: */
