import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Font;

public class ScoreBoard
{
    private CustomFont font;
    public int score;
    public int lines;
    public int high;
    public int level;
    private int width;
    private int height;
    private int gameType;

    public ScoreBoard(int w, int h)
    {
        reset(1);
        width = w;
        height = h;
        gameType = 0;
        font = CustomFont.getFont( 
                "/anmono.png",
                Font.SIZE_SMALL,
                Font.STYLE_PLAIN );
    }

    public void reset(int i)
    {
        score = 0;
        lines = 0;
        high = 0;
        level = i;
    }

    public void setLevel(int i)
    {
        level = i;
    }

    public void setGameType(int i)
    {
        gameType = i;
    }

    public void update(int l, int lines)
    {
        score = score + (level * lines);
        this.lines = this.lines + lines;
        if(this.lines >= level*10 && gameType == 0)level++;
    }

    public void getHigh()
    {
    }

    public void draw(Graphics g,int time)
    {
        g.setFont(Font.getFont(Font.FACE_MONOSPACE,Font.STYLE_BOLD,Font.SIZE_SMALL));
        String levelStr  = Integer.toString(level);

        switch(gameType){
            case 0:
                String scoreStr = Integer.toString(score);
                String linesStr = Integer.toString(lines);
                font.drawString(g,"SCORE",width/2 + 32,height/2 +4,Graphics.HCENTER|Graphics.TOP);
                font.drawString(g,scoreStr,width/2 + 32,height/2 + 11,Graphics.HCENTER|Graphics.TOP);
                font.drawString(g,"LINES",width/2 + 32,height/2 + 18,Graphics.HCENTER|Graphics.TOP);  	
                font.drawString(g,linesStr,width/2 + 32,height/2 +25,Graphics.HCENTER|Graphics.TOP); 
                font.drawString(g,"LEVEL",width/2 + 32,height/2 + 32,Graphics.HCENTER|Graphics.TOP);  	
                font.drawString(g,levelStr ,width/2 + 32,height/2+ 39,Graphics.HCENTER|Graphics.TOP);
                font.drawString(g,"HIGH",width/2 + 32,height/2 + 46,Graphics.HCENTER|Graphics.TOP);  	
                font.drawString(g,"00000" ,width/2 + 32,height/2 + 53,Graphics.HCENTER|Graphics.TOP);	
                break;
            case 1:
                String t = Integer.toString(time);
                font.drawString(g,"TIME",width/2 + 32,height/2 +4,Graphics.HCENTER|Graphics.TOP);
                font.drawString(g,t,width/2 + 32,height/2 + 11,Graphics.HCENTER|Graphics.TOP);
                font.drawString(g,"LEVEL",width/2 + 32,height/2 + 18,Graphics.HCENTER|Graphics.TOP);    
                font.drawString(g,levelStr,width/2 + 32,height/2 +25,Graphics.HCENTER|Graphics.TOP); 
                font.drawString(g,"BEST",width/2 + 32,height/2 + 32,Graphics.HCENTER|Graphics.TOP);    
                font.drawString(g,"100" ,width/2 + 32,height/2+ 39,Graphics.HCENTER|Graphics.TOP);  	
                break;
        }
    }
}
/* vim: set ts=4 sw=4 nu expandtab: */
