import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

public class NextPiece
{
    private Point[] points;
    private Matrix nextMatrix;
    private int color;

    public NextPiece(Point[] points,int c,Image i,Rectangle r)
    {
        this.points  = points;
        color = c;
        nextMatrix = new Matrix(r, 6, 5,i);
        nextMatrix.placePiece(points,color);
    }

    public void setPoints(Point p[],int c)
    {
        points = p;
        color = c;
        nextMatrix.clear();
        nextMatrix.placePiece(points,color);
    }

    public void draw(Graphics g)
    {
        nextMatrix.draw(g);
    }

    public int getColor()
    {
        return color;
    }
}
/* vim: set ts=4 sw=4 nu expandtab: */
