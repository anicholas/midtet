import javax.microedition.midlet.MIDlet;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;

public class Tetris extends MIDlet
{
    Board board;
    GameMenu menu;
    public static final int GAME = 0;
    public static final int MENU = 1;

    public Tetris()
    {
        menu = new GameMenu(this);
        board = new Board(this);
    }

    public void startApp()
    {
        new SplashScreen(Display.getDisplay(this), menu);
    }

    public void switchTo(int i)
    {
        Display d = Display.getDisplay(this);
        switch(i){
            case MENU:
                d.setCurrent(menu);
                break;
            default:
                board.setType (menu.getSelectedIndex(GameMenu.MENU_OPTIONS,GameMenu.OPTIONS_GAMETYPE));
                board.setLevel(menu.getSelectedIndex(GameMenu.MENU_OPTIONS,GameMenu.OPTIONS_GAMELEVEL));
                board.setSound(menu.getSelectedIndex(GameMenu.MENU_OPTIONS,GameMenu.OPTIONS_SOUND_TOGGLE));
                board.setMusic(menu.getSelectedIndex(GameMenu.MENU_OPTIONS,GameMenu.OPTIONS_MUSIC_TOGGLE));
                board.setVolume(menu.getSelectedIndex(GameMenu.MENU_OPTIONS,GameMenu.OPTIONS_MUSIC_VOLUME));
                board.reset();
                d.setCurrent(board);
        }
    }

    public void pauseApp() { }

    public void destroyApp(boolean unc) { }

}
/* vim: set ts=4 sw=4 nu expandtab: */
