import java.util.Vector;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;

public class GameMenuItem
{
    private String name;
    private Vector items;
    private int currentItem;
    private int type;
    private int scrollValue;
    private int scrollIncrement;
    public static final int TYPE_TOGGLE = 0;
    public static final int TYPE_SELECT = 1;
    public static final int TYPE_TEXTAREA = 2;
    public static final int TYPE_VSCROLL = 3;
    public static final int TOGGLE_LEFT = -1;
    public static final int TOGGLE_RIGHT = 1;

    public GameMenuItem(int type,String name)
    {
        this.type = type;
        this.name = name;
        scrollValue = 50;
        scrollIncrement = 10;
        items = new Vector();
        currentItem = 0;
    }

    public void addToggleItem(String s, int i)
    {
        items.insertElementAt(s,i);
    }

    public void toggle(int i)
    {
        if(type==TYPE_VSCROLL){
            int tscrollValue = scrollValue + i*(scrollIncrement);
            if(tscrollValue >=0 && tscrollValue <=100)scrollValue = tscrollValue;
            return;	
        }
        currentItem =currentItem + i;
        if(currentItem>(items.size()-1))currentItem = 0;
        if(currentItem<0)currentItem = items.size()-1;
    }

    public int getCurrentIndex()
    {
        if (type==TYPE_VSCROLL)return scrollValue;
        return currentItem;
    }

    public void setType(int i)
    {
        type = i;
    }

    public String getDisplayString()
    {
        if(items.size() <= 0){
            if(type==TYPE_VSCROLL){
                return name + " " + Integer.toString(scrollValue) + ":";
            }
            return name;
        }else{
            return (String)items.elementAt(currentItem);
        }

    }

    public void select()
    {
        if (type==TYPE_TOGGLE)toggle(1);
    }

}
/* vim: set ts=4 sw=4 nu expandtab: */
