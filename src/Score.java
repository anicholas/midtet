
import java.lang.System;
import java.lang.Runnable;
import java.lang.InterruptedException;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Graphics;

import javax.microedition.rms.RecordStore;
import javax.microedition.rms.RecordStoreException;

public class Score {

    /** Array of high scores for each game level. */
    private static short[] highScore = new short[6];
    /** Array of player names for each high score level. */
    private static String[] highScoreName = new String[6];
    /** Current score for this game. */
    private static RecordStore myStore;
    /** Is the internal score struct intialized? */
    private static boolean highScoresHaveBeenInit; /* = false; */

    private Score() {
    }

    /**
     * Initialize all high scores to 0.
     */
    private static void initializeScores()
    {
        /* Initialize the score store */
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(baos);
        byte[] b;

        try {
            try {
                dos.writeShort(0);
                dos.writeUTF("EMPTY");
                b = baos.toByteArray();
                dos.close();
            } catch (IOException ioe) {
                throw new RecordStoreException();
            }


            for (int i = 0; i < 6; i++) {
                myStore.addRecord(b, 0, b.length);
            }
        } catch (RecordStoreException rse) {
            closeHighScores();
        }
    }

    static void openHighScores()
    {
        try {
            myStore = RecordStore.openRecordStore("tethighScores", true);
            if (highScoresHaveBeenInit) return;

            if (myStore.getNumRecords() == 0) {
                initializeScores();
            } else {
                ByteArrayInputStream bais;
                DataInputStream      dis;
                byte                 data[];

                for (int i = 0; i < 6; i++) {
                    data = myStore.getRecord(i+1);
                    if (data != null) {
                        try {
                            bais = new ByteArrayInputStream(data);
                            dis  = new DataInputStream(bais);
                            highScore[i]     = dis.readShort();
                            highScoreName[i] = dis.readUTF();
                            dis.close();
                        } catch (IOException ioe) {
                        }
                    }
                }
            }
            highScoresHaveBeenInit = true;
        } catch (RecordStoreException rse) {
        }
    }

    static void closeHighScores()
    {
        if (myStore != null) {
            try {
                myStore.closeRecordStore();
            } catch (RecordStoreException frse) {
            }
            myStore = null;
        }
    }

    /**
     * Save high score for posterity.
     * @param level current game level
     * @param newScore current game score to be recorded
     * @param name current user name to be recorded
     */
    static void setHighScore(int level, int newScore, String name)
    {
        ByteArrayOutputStream baos;
        DataOutputStream      das;
        byte[]                data;

        if (newScore <= highScore[level]) {
            return;
        }

        try {
            try {
                baos = new ByteArrayOutputStream();
                das = new DataOutputStream(baos);
                das.writeShort((short)newScore);
                das.writeUTF(name);
                data = baos.toByteArray();
                das.close();
            } catch (IOException ioe) {
                throw new RecordStoreException();
            }

            if (myStore == null) {
                openHighScores();
                myStore.setRecord(level + 1, data, 0, data.length);
                closeHighScores();
            } else {
                myStore.setRecord(level + 1, data, 0, data.length);
            }
        } catch (RecordStoreException rse) {
        }

        highScore[level]     = (short)newScore;
        highScoreName[level] = name;
    }

    /**
     * Return the high score for a given level.
     * @param level current level for high score check
     * @return numeric value for highest score at the
     * requested level
     */
    static short getHighScore(int level)
    {
        if (!highScoresHaveBeenInit) {
            openHighScores();
            closeHighScores();
        }
        return highScore[level];
    }

    /**
     * Return the high score name for a given level.
     * @param level current level for high score check
     * @return name for highest score at the
     * requested level
     */
    static String getHighScoreName(int level) {
        if (!highScoresHaveBeenInit) {
            openHighScores();
            closeHighScores();
        }
        return highScoreName[level];
    }
}
/* vim: set ts=4 sw=4 nu expandtab: */
