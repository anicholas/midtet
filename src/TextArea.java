import java.util.Vector;
import javax.microedition.lcdui.Graphics;
import java.util.Enumeration;

public class TextArea
{
    private Vector text;
    private boolean useCustomFont;
    private CustomFont font;
    private int x,y,width,height,spacing;
    private int topLine; 
    private int maxLinesPerScreen;
    public static final int SCROLL_DOWN = 0;
    public static final int SCROLL_UP = 1;

    public TextArea(int x,int y,int w, int h,CustomFont f)
    {
        text = new Vector();
        useCustomFont = false;
        this.x = x;
        this.y = y;
        width = w;
        height = h;
        font = f;
        spacing = 2;
        topLine = 0;
    }

    public void addText(String s)
    {
        text.addElement(s);
    }

    public void draw(Graphics g)
    {
        int tx = x +width/2;
        int ty = y + spacing + font.getHeight();

        for (int i = topLine; i < text.size();i++){
            if((i - topLine) == getMaxNoumberOfLines())break;
            String s = (String)text.elementAt(i);
            font.drawString(g,s,tx,ty +(i-topLine)*(spacing + font.getHeight()),
                    Graphics.HCENTER|Graphics.TOP);
        }

        if(topLine!=0)font.drawString(g,"a",tx,y,Graphics.HCENTER|Graphics.TOP);    
        if(topLine + getMaxNoumberOfLines() < text.size())font.drawString(g,"b",tx,y+height,Graphics.HCENTER|Graphics.TOP);
    }

    public void scroll(int i)
    {
        switch(i){
            case SCROLL_DOWN:
                if(topLine + getMaxNoumberOfLines() <= text.size())topLine++;
                break;
            case SCROLL_UP:
                if(topLine !=0)topLine--;
                break;
        }
    }

    private int getMaxNoumberOfLines()
    {
        return height/(font.getHeight()+spacing);
    }
}
/* vim: set ts=4 sw=4 nu expandtab: */
