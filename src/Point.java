public class Point
{
    public int x;
    public int y;

    public Point(int x, int y)
    {
        setLocation(x,y);
    }

    public Point()
    {
        setLocation(0,0);
    }

    public void setLocation(int x, int y)
    {
        this.x = x;
        this.y = y;	
    }
}
/* vim: set ts=4 sw=4 nu expandtab: */
