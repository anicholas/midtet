import javax.microedition.lcdui.game.GameCanvas;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Font;
import java.io.IOException;
import javax.microedition.lcdui.Image;


public class GameMenu extends GameCanvas implements Runnable{

    private int height;
    private int width;
    private Tetris      parent;
    private int         selectedMenuCommand;
    private Display     display;
    private CustomFont  font,font2,font3;
    private GameMenuItem[][] menus;
    private TextArea textArea; 
    private int[] highScores = new int[6];
    private String[] highNames = new String[6];

    //display
    private final int         MENU_HOME      = 0;
    public static final int   MENU_OPTIONS   = 1;
    private final int         MENU_HELP      = 3;
    private final int         MENU_HIGHSCORE = 2;
    private final int         MENU_NEWHIGH   = 4;
    private final int         MENU_ZLAST     = 5;
    private int               currentMenu;

    //default menus
    private final int   HOME_START = 0;
    private final int   HOME_OPTIONS = 1;
    private final int   HOME_HIGHSCORES = 2;
    private final int   HOME_ABOUT = 3;
    private final int   HOME_EXIT = 4;
    private final int   HOME_ZLAST = 5;

    //sounds menus
    public static final int   OPTIONS_GAMETYPE = 0;
    public static final int   OPTIONS_GAMELEVEL = 1;
    public static final int   OPTIONS_SOUND_TOGGLE = 2;
    public static final int   OPTIONS_MUSIC_TOGGLE = 3;
    public static final int   OPTIONS_MUSIC_VOLUME = 4;
    public static final int   OPTIONS_BACK = 5;
    private final int         OPTIONS_ZLAST        = 6;

    //sound menus
    public static final int   TOGGLE_OFF = 1;
    public static final int   TOGGLE_ON =  0;

    private Thread thread;
    private StarField stars;
    private boolean animate;
    private Graphics graphics;

    public GameMenu(Tetris parent)
    {
        super(false);
        setFullScreenMode(true);
        graphics = getGraphics();
        graphics.setColor( 0,  0, 0 );
        graphics.fillRect( 0, 0, getWidth(), getHeight() );
        initHighScores();
        width = getWidth();
        height = getHeight();
        animate = false;
        this.parent = parent;
        font = CustomFont.getFont(
                "/anmono.png",
                Font.SIZE_SMALL,
                Font.STYLE_PLAIN );
        font2 = CustomFont.getFont(
                "/anmono2.png",
                Font.SIZE_SMALL,
                Font.STYLE_PLAIN );
        font3 = CustomFont.getFont(
                "/tetrisMono.png",
                Font.SIZE_SMALL,
                Font.STYLE_PLAIN );
        createMenus();
        createTextAreas();
        selectedMenuCommand = 0;
        stars = new StarField(100,10,getWidth(),getHeight());
        this.display = Display.getDisplay(parent);
    }

    private void createTextAreas()
    {
        int height = getHeight();
        int width = getWidth();
        int left = (width - 128)/2;
        int top = (height - 128)/2 + 20;
        textArea = new TextArea(left,top,128,98,font);
        textArea.addText("TETRIS");
        textArea.addText("V.1.01");
        textArea.addText("2004/2005 A.NICHOLAS");
        textArea.addText(".");
        textArea.addText("GRAPHICS BASED ON ATARI");
        textArea.addText("ARCADE VERSION");
        textArea.addText(".");
        textArea.addText("CONTROLS");
        textArea.addText("4/LEFT:     MOVE LEFT");
        textArea.addText("6/RIGHT:   MOVE RIGHT");
        textArea.addText("5/SELECT:      ROTATE");
        textArea.addText("8/DOWN      DROP DOWN");
        textArea.addText("2/UP    COMPLETE DROP");
        textArea.addText(".");
        textArea.addText(".");
        textArea.addText("GAME TYPES");
        textArea.addText(".");
        textArea.addText("TYPE A");
        textArea.addText("START WITH NO LINES");
        textArea.addText("AND TRY TO GET THE");
        textArea.addText("HIGHEST SCORE BY");
        textArea.addText("REMOVING THE MOST LINES");
        textArea.addText(".");
        textArea.addText("TYPE B");
        textArea.addText("CLEAR ALL GRAY LINES");
        textArea.addText("TO COMPLETE THE GAME");
        textArea.addText(".");
        textArea.addText("SCORING");
        textArea.addText("1 LINE =  1 X LEVEL");
        textArea.addText("2 LINES = 3 X LEVEL");
        textArea.addText("3 LINES = 7 X LEVEL");
        textArea.addText("4 LINES = 15 X LEVEL");
    }

    public synchronized void paint()
    {
        if(currentMenu == MENU_HIGHSCORE){
            animate = true;
        }else{
            animate = false;
        }

        graphics.setColor(0,0,0);
        graphics.fillRect(0,0,width,height);
        String temp = "OPTIONS";

        switch(currentMenu){
            case MENU_HOME: 
                temp = "TETRIS";
            case MENU_OPTIONS:
                int mx = width/2; 
                int my = height/2 - ((menus[currentMenu].length)/2)*7;
                int tiy = my - font3.getHeight();

                font3.drawString(graphics,temp,mx,tiy,Graphics.HCENTER|Graphics.TOP); 
                for (int i = 0; i < menus[currentMenu].length;i++) {
                    String s = menus[currentMenu][i].getDisplayString();	
                    if(i==selectedMenuCommand){
                        s = "[ " + s + " ]"; 
                    }
                    font.drawString(graphics,s,mx,my +(i*7),Graphics.HCENTER|Graphics.TOP);
                }
                break;
            case MENU_HELP:
                int height = getHeight();
                int top = (height - 128)/2 + 4;
                int ix = width/2;
                font3.drawString(graphics,"HELP",ix,top,Graphics.HCENTER|Graphics.TOP); 
                textArea.draw(graphics);
                break;      
            case MENU_HIGHSCORE:
                drawHighScores(); 
                break;
        }
        flushGraphics();
    }

    private void initHighScores()
    {
        try {
            Score.openHighScores();
            for(int i = 0; i < 6; i++){
                highScores[i] = Score.getHighScore(i);
                highNames[i] = Score.getHighScoreName(i).toUpperCase();
            }
            Score.closeHighScores();
        } catch (Exception e) {
            for(int i = 0; i < 6; i++){
                highScores[i] = 0;
                highNames[i] = "deroB";
            }
        }
    }

    private void drawHighScores()
    {
        int y = (height - 7*8 - font3.getHeight() - 2)/2;
        int x = width/2;

        stars.draw(graphics);
        font3.drawString(graphics,"BEST",x,y,Graphics.HCENTER|Graphics.TOP);
        y = y +  font3.getHeight() + 2;
        font2.drawString(graphics,"GAMETYPE A",x,y,Graphics.HCENTER|Graphics.TOP);
        y=y +7;
        String s = highNames[0] + " " + Integer.toString(highScores[0]);
        font.drawString(graphics,s,width/2,y,Graphics.HCENTER|Graphics.TOP);
        y=y +10;
        font2.drawString(graphics,"GAMETYPE B",x,y,Graphics.HCENTER|Graphics.TOP);
        y=y +7;
        for(int i = 1; i < 6; i++){
            if(highScores[i] == 0){
                s = "LEVEL" +Integer.toString(i) + " " + "INCOMPLETE";
                font.drawString(graphics,s,x,y,Graphics.HCENTER|Graphics.TOP); 
            }else{
                s = "LEVEL" +Integer.toString(i) + " " + highNames[i] + " " +
                    Integer.toString(highScores[i]) + "SECS"; 
                font.drawString(graphics,s,x,y,Graphics.HCENTER|Graphics.TOP); 
            } 
            y=y +7; 
        }
    }

    protected void hideNotify()
    {
        thread = null;
    }

    public void run()
    {
        Thread mythread = Thread.currentThread();
        while (mythread == thread) {
            try {
                try {
                    mythread.sleep(20);
                    if(animate)paint();
                } catch (java.lang.InterruptedException e) {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void createMenus()
    {
        /////////////////////////////////////////
        //home menu
        /////////////////////////////////////////
        menus = new GameMenuItem[MENU_ZLAST][];
        menus[MENU_HOME] = new GameMenuItem[HOME_ZLAST];
        menus[MENU_OPTIONS] = new GameMenuItem[OPTIONS_ZLAST];
        menus[MENU_HELP] = new GameMenuItem[1];//home button
        menus[MENU_HIGHSCORE] = new GameMenuItem[1];//home button

        /////////////////////////////////////////
        //home menu
        /////////////////////////////////////////
        menus[MENU_HOME][HOME_START] = new GameMenuItem(GameMenuItem.TYPE_SELECT, "START");
        menus[MENU_HOME][HOME_OPTIONS] = new GameMenuItem(GameMenuItem.TYPE_SELECT, "OPTIONS");
        menus[MENU_HOME][HOME_HIGHSCORES] = new GameMenuItem(GameMenuItem.TYPE_SELECT, "HIGHSCORES");
        menus[MENU_HOME][HOME_ABOUT] = new GameMenuItem(GameMenuItem.TYPE_SELECT, "ABOUT");
        menus[MENU_HOME][HOME_EXIT] = new GameMenuItem(GameMenuItem.TYPE_SELECT, "EXIT");

        /////////////////////////////////////////
        //option menu
        /////////////////////////////////////////
        menus[MENU_OPTIONS][OPTIONS_GAMETYPE]
            = new GameMenuItem(GameMenuItem.TYPE_TOGGLE, "GAME TYPE");
        menus[MENU_OPTIONS][OPTIONS_GAMETYPE].addToggleItem("GAME TYPE A",0);
        menus[MENU_OPTIONS][OPTIONS_GAMETYPE].addToggleItem("GAME TYPE B",1);

        menus[MENU_OPTIONS][OPTIONS_GAMELEVEL]
            = new GameMenuItem(GameMenuItem.TYPE_TOGGLE, "START LEVEL");
        menus[MENU_OPTIONS][OPTIONS_GAMELEVEL].addToggleItem("LEVEL 1",0);
        menus[MENU_OPTIONS][OPTIONS_GAMELEVEL].addToggleItem("LEVEL 2",1);
        menus[MENU_OPTIONS][OPTIONS_GAMELEVEL].addToggleItem("LEVEL 3",2);
        menus[MENU_OPTIONS][OPTIONS_GAMELEVEL].addToggleItem("LEVEL 4",3);
        menus[MENU_OPTIONS][OPTIONS_GAMELEVEL].addToggleItem("LEVEL 5",4);

        menus[MENU_OPTIONS][OPTIONS_SOUND_TOGGLE]
            = new GameMenuItem(GameMenuItem.TYPE_TOGGLE, "-");
        menus[MENU_OPTIONS][OPTIONS_SOUND_TOGGLE].addToggleItem("SOUND ON",0);
        menus[MENU_OPTIONS][OPTIONS_SOUND_TOGGLE].addToggleItem("SOUND OFF",1);

        menus[MENU_OPTIONS][OPTIONS_MUSIC_TOGGLE]
            = new GameMenuItem(GameMenuItem.TYPE_TOGGLE, "-");
        menus[MENU_OPTIONS][OPTIONS_MUSIC_TOGGLE].addToggleItem("MUSIC ON",0);
        menus[MENU_OPTIONS][OPTIONS_MUSIC_TOGGLE].addToggleItem("MUSIC OFF",1);
        menus[MENU_OPTIONS][OPTIONS_MUSIC_VOLUME]
            = new GameMenuItem(GameMenuItem.TYPE_VSCROLL, "VOLUME");

        menus[MENU_OPTIONS][OPTIONS_BACK] = new GameMenuItem(GameMenuItem.TYPE_SELECT, "BACK TO MENU");

        /////////////////////////////////////////
        //other menus
        /////////////////////////////////////////
        menus[MENU_HELP][0] = new GameMenuItem(GameMenuItem.TYPE_SELECT, "BACK");
        menus[MENU_HIGHSCORE][0] = new GameMenuItem(GameMenuItem.TYPE_SELECT, "BACK");
    }

    protected void showNotify()
    {
        thread = new Thread(this);
        thread.start();
        initHighScores();
        paint();
    }

    public int getSelectedIndex(int a,int b)
    {
        return menus[a][b].getCurrentIndex();
    }

    public void keyPressed(int code)
    {
        int game = getGameAction(code);

        switch (game) {
            case Canvas.UP:
                if(currentMenu>=MENU_HELP){
                    textArea.scroll(TextArea.SCROLL_UP);
                    break;
                }
                if(selectedMenuCommand==0){
                    selectedMenuCommand = menus[currentMenu].length-1;
                }else{
                    selectedMenuCommand--;
                }
                break;
            case Canvas.DOWN:
                if(currentMenu>=MENU_HELP){
                    textArea.scroll(TextArea.SCROLL_DOWN);
                    break;
                }
                if(selectedMenuCommand==(menus[currentMenu].length-1)){
                    selectedMenuCommand = 0;
                }else{
                    selectedMenuCommand++;
                }
                break;
            case Canvas.LEFT:
                menus[currentMenu][selectedMenuCommand].toggle(-1);
                break;
            case Canvas.RIGHT:
                menus[currentMenu][selectedMenuCommand].toggle(1);
                break;
            case Canvas.FIRE:
                menus[currentMenu][selectedMenuCommand].select();
                switch(currentMenu){
                    case MENU_HOME:
                        switch(selectedMenuCommand){
                            case HOME_START:
                                parent.switchTo(Tetris.GAME);
                                break;
                            case HOME_EXIT:
                                parent.notifyDestroyed();
                                break;
                            default:
                                currentMenu = selectedMenuCommand;
                                selectedMenuCommand = 0; 
                                break;
                        }
                        break;
                    case MENU_OPTIONS:
                        switch(selectedMenuCommand){
                            case OPTIONS_BACK:
                                currentMenu = MENU_HOME;
                                selectedMenuCommand = 0;
                                break;
                        }
                        break;
                    default:
                        currentMenu = selectedMenuCommand;
                        selectedMenuCommand = 0;
                }
        }
        paint();
    }
}

/* vim: set ts=4 sw=4 nu expandtab: */
