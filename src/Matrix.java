import javax.microedition.lcdui.game.Sprite;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.Graphics;
import java.util.Random;

public class Matrix
{
    private int             pieceWidth,pieceHeight;
    private int             noOfRows,noOfColumns;
    private Rectangle       bounds;
    private MatrixSlot[][]  slot;
    public static final int NO_OF_ROWS = 20;
    public static final int NO_OF_COLUMNS = 10;
    public static final int TOP_GUTTER = 2;
    private final int LK_BOTTOM = 0;
    private final int LK_SIDE   = 1;
    private final int LK_FULL   = 2;
    private final int LK_TOP   = 3;

    public Matrix(Rectangle b,Image i)
    {
        this(b,NO_OF_COLUMNS,NO_OF_ROWS,i);
    }

    public Matrix(Rectangle b,int c, int r,Image i)
    {
        noOfRows    = r + TOP_GUTTER;
        noOfColumns = c;
        bounds      = b;
        pieceWidth  = b.width/c;
        pieceHeight = b.height/r;
        createSlots(i);
        clear();
    }

    private void createSlots(Image i)
    {
        slot = new MatrixSlot[noOfColumns][noOfRows];
        for(int r = 0; r < noOfRows; r++ ){
            for(int c = 0; c < noOfColumns; c++){
                int sx = bounds.x + (c * pieceWidth);
                int sy = bounds.y + ((r - TOP_GUTTER) * pieceHeight); 
                slot[c][r] = new MatrixSlot(i,6,6);
                slot[c][r].setPosition(sx,sy);
            }
        }
    }

    public void placePiece(Point[] p,int color)
    {
        if(p[0].y>=TOP_GUTTER)slot[p[0].x][p[0].y].fill(color);
        if(p[1].y>=TOP_GUTTER)slot[p[1].x][p[1].y].fill(color);
        if(p[2].y>=TOP_GUTTER)slot[p[2].x][p[2].y].fill(color);
        if(p[3].y>=TOP_GUTTER)slot[p[3].x][p[3].y].fill(color);
    }

    public void fillSlot(int c, int r,int color)
    {
        slot[c][r].fill(color);
    }

    public int checkForCompleteRows()
    {
        int rc = 0;
        for (int i = 0; i < noOfRows; i++){
            if(rowIsFull(i)){
                clearRow(i);
                rc++;
            }
        }
        return rc;
    }

    private boolean rowIsFull(int r)
    {
        int i = 0;
        for (int c = 0; c < noOfColumns; c++){
            if(slot[c][r].isVisible())i++;
        }
        if(i==noOfColumns){
            return true;
        }else{
            return false;
        }
    }

    public void removePiece(Point[] p)
    {
        slot[p[0].x][p[0].y].setVisible(false);
        slot[p[1].x][p[1].y].setVisible(false);
        slot[p[2].x][p[2].y].setVisible(false);
        slot[p[3].x][p[3].y].setVisible(false);
    }

    public int canPlacePiece(Point[] p)
    {
        if(isOnBottom(p)) {
            return LK_BOTTOM;
        }

        if(isOffSide(p)) {
            return LK_SIDE;
        }

        try {
            if(slot[p[0].x][p[0].y].isVisible()) {
                return LK_FULL;
            }

            if(slot[p[1].x][p[1].y].isVisible()) {
                return LK_FULL;
            }

            if(slot[p[2].x][p[2].y].isVisible()) {
                return LK_FULL;
            }

            if(slot[p[3].x][p[3].y].isVisible()) {
                return LK_FULL;
            }
        } catch(Exception e) {
            return LK_SIDE;
        }

        return -1;
    }

    public boolean isOnBottom(Point[] p)
    {
        for(int r = 0; r < 4; r++) {
            if(p[r].y >= noOfRows) {
                return true;
            }
        }
        return false;
    }

    public boolean isOffTop(Point[] p)
    {
        for(int r = 0; r < 4; r++) {
            if(p[r].y < TOP_GUTTER) {
                return true;
            }
        }
        return false;
    }

    public void randomFillBottomRows(int i)
    {
        Random random = new Random(); 
        for(int c = noOfColumns-1; c >=0; c--){
            for(int r = noOfRows-1; r >(noOfRows-i-1); r--){
                int ok = Math.abs(random.nextInt()%2);
                if(ok==1)slot[c][r].fill(Piece.X_PIECE);
            }
        }
    }

    public boolean isOffSide(Point[] p)
    {
        for(int r = 0; r < 4; r++) {
            if((p[r].x < 0) || (p[r].x > noOfColumns)) {
                return true;
            }
        }
        return false;
    }

    public void clear()
    {
        for(int r = 0; r < noOfRows; r++) {
            for(int c = 0; c < noOfColumns; c++) {
                slot[c][r].setVisible(false);
            }
        }
    }

    public void clearRow(int r)
    {
        for(int c = 0; c < noOfColumns; c++) {
            slot[c][r].setVisible(false);
        }
        moveMatrixDown(r);
    }

    public void moveMatrixDown(int i)
    {
        for(int r = i; r > 0; r--) {
            for(int c = 0; c < noOfColumns; c++) {
                slot[c][r] = slot[c][r-1].cloneTo(slot[c][r]);
            }
        }
    }

    public void draw(Graphics g)
    {
        for(int r = 0; r < noOfRows; r++) {
            for(int c = 0; c < noOfColumns; c++) {
                slot[c][r].paint(g);
            }
        }
    }
}

class MatrixSlot extends Sprite
{
    private int color;

    public MatrixSlot(Image i,int x, int y)
    {
        super(i,x,y);
        color = 0;
        setFrame(color);
    }

    public MatrixSlot(MatrixSlot m,int x, int y)
    {
        super((Sprite)m);
        setPosition(x,y);
        color = m.getColor();
        setFrame(color);
    }

    public void setColor(int c)
    {
        setFrame(c);
        this.color = c;
    }

    public int getColor()
    {
        return color;
    }

    public void fill(int c)
    {
        setColor(c);
        setVisible(true);
    }

    public MatrixSlot cloneTo(MatrixSlot m)
    {
        return new MatrixSlot(this,m.getX(),m.getY()); 
    }

}

class Rectangle
{
    public int x,y,width,height;

    public Rectangle(int x, int y, int width, int height)
    {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
}
/* vim: set ts=4 sw=4 nu expandtab: */
