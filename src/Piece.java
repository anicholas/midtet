import java.util.Random;

public class Piece{

    Board board;
    private Point[][] tetraminoArray;
    private Point[] tetramino;
    private Point location;
    private int rotation;

    /**
     * L tetraminoArray layout
     * <pre>
     *  an 'L' tetraminoArray has a layout as such:
     *  0. []||[]  1. []    2.   []   3.   []
     *       []       ||[]     []||[]    []||
     *                []                   []
     * </pre>
     */
    public static final int L_PIECE = 0;
    /**
     * L tetraminoArray layout
     * <pre>
     *  an 'L' tetraminoArray has a layout as such:
     *  0. []||[]  1. []    2.   []   3.   []
     *       []       ||[]     []||[]    []||
     *                []                   []
     * </pre>
     */
    public static final int P_PIECE = 1;
    /**
     * L tetraminoArray layout
     * <pre>
     *  an 'L' tetraminoArray has a layout as such:
     *  0. []||[]  1. []    2.   []   3.   []
     *       []       ||[]     []||[]    []||
     *                []                   []
     * </pre>
     */
    public static final int T_PIECE = 2;
    /**
     * T tetraminoArray layout
     * <pre>
     *  a 'T' tetraminoArray has a layout as such:
     *  0. []||[]  1. []    2.   []   3.   []
     *       []       ||[]     []||[]    []||
     *                []                   []
     * </pre>
     */
    public static final int O_PIECE = 3;
    /**
     * an 'O' tetraminoArray layout
     * <pre>
     *  an 'O' tetraminoArray has a layout as such:
     *  0. []||[]  1. []    2.   []   3.   []
     *       []       ||[]     []||[]    []||
     *                []                   []
     * </pre>
     */
    public static final int I_PIECE = 4;
    /**
     * I tetraminoArray layout
     * <pre>
     *  an 'I' tetraminoArray has a layout as such:
     *  0. []||[]  1. []    2.   []   3.   []
     *       []       ||[]     []||[]    []||
     *                []                   []
     * </pre>
     */
    public static final int S_PIECE = 5;
    /**
     * Z tetraminoArray layout
     * <pre>
     *  a 'Z' tetraminoArray has a layout as such:
     *  0. []||[]  1. []    2.   []   3.   []
     *       []       ||[]     []||[]    []||
     *                []                   []
     * </pre>
     */
    public static final int Z_PIECE = 6;
    public static final int X_PIECE = 7;

    /** current colour of piece */
    private int color;

    public static final int ROTATE_LEFT = 1;
    public static final int ROTATE_RIGHT = -1;

    /**
     * create a Tetramino of specified layout
     * @param  i  tertramino type (int between 0 and 6)
     * @see    #L_PIECE
     * @see    #P_PIECE
     * @see    #T_PIECE
     * @see    #O_PIECE
     * @see    #I_PIECE
     * @see    #S_PIECE
     * @see    #Z_PIECE
     */
    public Piece(int i)
    {
        init(i);
    }

    /**
     * create a Tetramino of a random type
     */
    public Piece()
    {
        Random random = new Random(); 
        int i = Math.abs(random.nextInt()%7);
        init(i);
    }

    private void init(int i)
    {
        color = i;
        tetraminoArray = createSetout(i);
        tetramino = new Point[4];
        setRotationIndex(0);
        location = new Point();
        setLocation(4,1);
        relocate(0,0,0);
    }

    /*
     * return the tetraminoArray as defined by i
     * @see #tetraminoArray
     */
    private Point[][] createSetout(int i)
    {
        switch (i){
            case L_PIECE: return createLSetout();
            case P_PIECE: return createPSetout();
            case T_PIECE: return createTSetout();
            case O_PIECE: return createOSetout();
            case I_PIECE: return createISetout();
            case S_PIECE: return createSSetout();
            case Z_PIECE: return createZSetout();
            default: return null;
        }
    }

    /**
     * returns the next roation index if the tetraminoArray was
     * to be rotated.
     * @param  i  an int difining the number of turns
     * @see    #ROTATE_LEFT
     * @see    #ROTATE_RIGHT
     * @see    #rotation
     */
    public int getRotationIndex(int i)
    {
        int r = rotation + i;
        if(r<0)r = tetraminoArray.length-1;
        if(r>tetraminoArray.length-1)r = 0;
        return r;
    }

    public void setRotationIndex(int i)
    {
        rotation = i;
    }

    public int getRotation()
    {
        return rotation;
    }

    public int getColor()
    {
        return color;
    }

    public Point getLocation()
    {
        return location;
    }

    public void setLocation(int x, int y)
    {
        location.x = x;
        location.y = y;
    }

    public Point[] getCurrentTetramino()
    {
        return tetramino;
    }

    public Point[] testRelocate(int r,int dx, int dy)
    {
        Point[] result = new Point[4];
        try{
            int x = location.x + dx;
            int y = location.y + dy;
            result[0] = new Point(x + tetraminoArray[r][0].x,y + tetraminoArray[r][0].y);
            result[1] = new Point(x + tetraminoArray[r][1].x,y + tetraminoArray[r][1].y);
            result[2] = new Point(x + tetraminoArray[r][2].x,y + tetraminoArray[r][2].y);
            result[3] = new Point(x + tetraminoArray[r][3].x,y + tetraminoArray[r][3].y);
        }catch(ArrayIndexOutOfBoundsException a){
            return null;
        }
        return result;
    }

    public void relocate(int r,int dx, int dy)
    {
        setRotationIndex(r);
        tetramino = testRelocate(r,dx,dy);
        location.setLocation(location.x + dx,location.y + dy);
    }

    /*
     * create an T tetraminoArray array
     * <pre>
     *  creates a 'T' tetraminoArray in an array as such:
     *  0. []||[]  1. []    2.   []   3.   []
     *       []       ||[]     []||[]    []||
     *                []                   []
     * </pre>
     */
    private final Point[][] createTSetout()
    {
        Point[][] p = {
            {new Point(0,0),new Point(-1,0),new Point(1,0),new Point(0,1)},
            {new Point(0,0),new Point(1,0),new Point(0,-1),new Point(0,1)},
            {new Point(0,0),new Point(-1,0),new Point(0,-1),new Point(1,0)},
            {new Point(0,0),new Point(-1,0),new Point(0,-1),new Point(0,1)}
        };
        return p;
    }

    /*
     * create an Z tetraminoArray array
     * <pre>
     *  creates a 'Z' tertramino in an array as such:
     *  0.   [][]  1.   []
     *         [][]   [][]
     *                []
     * </pre>
     */
    private final Point[][] createZSetout()
    {
        Point[][] p = {
            {new Point(0,0),new Point(-1,0),new Point(0,1),new Point(1,1)},
            {new Point(0,0),new Point(1,0),new Point(0,1),new Point(1,-1)}
        };
        return p;
    }

    /**
     * create an S tetraminoArray array
     * <pre>
     *  creates a 'S' tertramino in an array as such:
     *  0.   [][]  1. []
     *     [][]       [][]
     *                  []
     * </pre>
     */
    private final Point[][] createSSetout()
    {
        Point[][] p = {
            {new Point(0,0),new Point(1,0),new Point(-1,1),new Point(0,1)},
            {new Point(0,0),new Point(1,0),new Point(0,-1),new Point(1,1)}
        };
        return p;
    }

    /**
     * FIXME
     * create I tetraminoArray array
     * <pre>
     *  creates a 'I' tertramino in an array as such:
     *  0.[][][][]  1.[]  2.[][][][]  3.[]
     *                []                []
     *                []                []
     *                []                []
     * </pre>
     */
    private final Point[][] createISetout()
    {
        Point[][] p = {
            {new Point(-1,0),new Point(0,0),new Point(1,0),new Point(2,0)},
            {new Point(0,-2),new Point(0,-1),new Point(0,0),new Point(0,1)},
            {new Point(-1,0),new Point(0,0),new Point(1,0),new Point(2,0)},
            {new Point(0,-2),new Point(0,-1),new Point(0,0),new Point(0,1)}
        };
        return p;
    }

    /**
     * create O tetraminoArray array
     * <pre>
     *  create a 'O' tertramino in an array as such:
     *  0. [][]
     *     [][]
     *
     * </pre>
     */
    private final Point[][] createOSetout()
    {
        Point[][] p = {
            {new Point(0,0),new Point(1,0),new Point(1,1),new Point(0,1)}
        };
        return p;
    }

    /**
     * create L tetraminoArray array
     * <pre>
     *  creates a 'L' tertramino in an array as such:
     *  0. [][]  1. [][][]  2.[]    3.     []
     *       []     []        []       [][][]
     *       []               [][]
     * </pre>
     */
    private final Point[][] createLSetout()
    {
        Point[][] p = {
            {new Point(0,0),new Point(-1,-1),new Point(0,-1),new Point(0,1)},
            {new Point(0,0),new Point(1,0),new Point(-1,0),new Point(-1,1)},
            {new Point(0,0),new Point(0,-1),new Point(0,1),new Point(1,1)},
            {new Point(0,0),new Point(-1,0),new Point(1,0),new Point(1,-1)}
        };
        return p;
    }

    /**
     * create P tetraminoArray array
     * <pre>
     *  create a 'P' tertramino in an array as such:
     *  0. [][]  1. []      2.  []  3. [][][]
     *     []       [][][]      []         []
     *     []                 [][]
     * </pre>
     */
    private final Point[][] createPSetout()
    {
        Point[][] p = {
            {new Point(0,0),new Point(0,1),new Point(0,-1),new Point(1,-1)},
            {new Point(0,0),new Point(-1,0),new Point(-1,-1),new Point(1,0)},
            {new Point(0,0),new Point(0,-1),new Point(0,1),new Point(-1,1)},
            {new Point(0,0),new Point(-1,0),new Point(1,0),new Point(1,1)}
        };
        return p;
    }
}
/* vim: set ts=4 sw=4 nu expandtab: */
